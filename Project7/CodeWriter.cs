﻿using System;
using System.IO;
using System.Collections.Generic;

namespace VirtualMachine
{
	/// <summary>
	/// Translates VM commands into Hack assembly code.
	/// </summary>
	public class CodeWriter
	{
		private StreamWriter _fstream;
		private int _romAddress;
		private static int _labelCounter = 1;
		// Starting memory addresses for the temp and pointer virtual segments.
		private const string PointerLoc = "3";
		private const string TempLoc = "5";
		private Dictionary<string, string> _translator;
		// The current file being translated.
		private string _currentFile;
		private string _currentFunction;

		/// <summary>
		/// Initializes a new instance of the <see cref="VirtualMachine.CodeWriter"/> class.
		/// </summary>
		/// <param name="filename">Filename.</param>
		public CodeWriter(string filename)
		{
			//_fstream = new StreamWriter( new FileStream(filename, FileMode.OpenOrCreate));
			_fstream = new StreamWriter(filename, false);
			_romAddress = 0;
			_translator = new Dictionary<string, string>()
			{ 
				// Add all binary arithmetic commands.
				{"add", "+"},
				{"sub", "-"},
				{"and", "&"},
				{"or", "|"},
				// A 'not'  is applied to the jump condition in 'eq', 'lt', and 'gt'.
				{"eq", "JEQ"},
				{"lt", "JGT"},
				{"gt", "JLT"},
				// Add all virtual memory segments.
				{"local", "LCL"},
				{"argument", "ARG"},
				{"this", "THIS"},
				{"that", "THAT"},
				{"pointer", "3"},
				{"temp", "5"}
			};

			// For some reason the init code causes comparison failures.
			//WriteBootstrap();
		}

		/// <summary>
		/// Sets the name of the current VM file.
		/// </summary>
		/// <param name="filename">Filename.</param>
		public void SetFileName(string filename)
		{
			_currentFile = filename;
		}

		/// <summary>
		/// Writes the arithmetic.
		/// </summary>
		/// <param name="command">Command.</param>
		public void WriteArithmetic(string command)
		{
			switch (command)
			{
				case "add":
				case "sub":
				case "and":
				case "or":
					// Translation is the same for binary operators, but with different symbols.
					WriteBinaryOp(_translator[command]);
					break;
				case "not":
					WriteUnaryOp();
					break;
				case "neg":
					WriteUnaryOp();
					// Finish 2's complement.
					_fstream.WriteLine("M=M+1");
					_romAddress++;
					break;
				case "eq":
				case "lt":
				case "gt":
					WriteInequality(_translator[command]);
					break;
					
			}
		}

		/// <summary>
		/// Writes the assembly code that is the translation of the given push or pop command.
		/// </summary>
		/// <param name="ctype">Ctype.</param>
		/// <param name="segment">Segment.</param>
		/// <param name="index">Index.</param>
		public void WritePushPop(CommandType ctype, string segment, string index)
		{
			// Push.
			if (ctype == CommandType.C_PUSH)
			{
				switch (segment)
				{
					case "constant":
						WritePushConstant(index);
						break;
					case "static":
						WritePushStatic(index);
						break;
					default:
						WritePushMemory(_translator[segment], index);
						break;
				}
			}
			// Pop.
			else
			{
				if (segment == "static")
					WritePopToStatic(index);
				else
					WritePopToMem(_translator[segment], index);
			}
		}

		/// <summary>
		/// Writes the assembly code for the VM initialization,
		/// or bootstrap.
		/// This is WriteInit in the API.
		/// </summary>
		private void WriteBootstrap() 
		{
			// Set SP=256.
			_fstream.WriteLine("@256");
			_fstream.WriteLine("D=A");
			_fstream.WriteLine("@SP");
			_fstream.WriteLine("M=D");
			_romAddress += 4;
			WriteCall("Sys.init", "0");
		}

		/// <summary>
		/// Writes the assembly code that effects the label command.
		/// </summary>
		/// <param name="label">Label.</param>
		public void WriteLabel(string label)
		{
			_fstream.WriteLine("(" + GetProperLabel(label) + ")");
		}

		/// <summary>
		/// Writes the assembly code that effects the goto command.
		/// </summary>
		/// <param name="label">Label.</param>
		public void WriteGoto(string label)
		{
			_fstream.WriteLine("@" + GetProperLabel(label));
			_fstream.WriteLine("0;JMP");
			_romAddress += 2;
		}

		/// <summary>
		/// Writes if.
		/// If the top of the stack is true, jump to the label.
		/// </summary>
		/// <param name="label">Label.</param>
		public void WriteIf(string label)
		{
			WritePopD();
			_fstream.WriteLine("@" + GetProperLabel(label));
			_fstream.WriteLine("D;JNE");
			_romAddress += 2;
		}

		/// <summary>
		/// Writes the assembly code that does the call VM command in assembly.
		/// This means it saves the current function's frame to the stack,
		/// repositions the local (LCL) and argument (ARG) pointers,
		/// and transfers control (goto/jumps) to the called function.
		/// </summary>
		/// <param name="fname">Fname.</param>
		/// <param name="numargs">Numargs.</param>
		public void WriteCall(string fname, string numargs)
		{
			WritePushConstant((_romAddress + 40).ToString());
			// Save the current frame's state.
			WritePushPointer("LCL");
			WritePushPointer("ARG");
			WritePushPointer("THIS");
			WritePushPointer("THAT");
			_fstream.WriteLine("@SP");
			_fstream.WriteLine("D=M");
			_fstream.WriteLine("@LCL");
			_fstream.WriteLine("M=D");
			// A = ARG adjustment.
			_fstream.WriteLine("@" + (Int32.Parse(numargs) + 5));
			// D = SP - ARG adjustment.
			_fstream.WriteLine("D=D-A");
			_fstream.WriteLine("@ARG");
			// ARG = SP - (numargs + 5).
			_fstream.WriteLine("M=D");
			_fstream.WriteLine("@" + fname);
			// Transfer control to the function.
			_fstream.WriteLine("0;JMP");
			_romAddress += 10;
			_fstream.WriteLine("(RIP" + _romAddress + ")");
		}

		/// <summary>
		/// Writes assembly code that effects the return VM command.
		/// Reposition the return value on to the top of the stack,
		/// restore the frame of the calling function, 
		/// and transfer control (goto/jump) back to the calling function via
		/// the return address saved to the stack.
		/// </summary>
		public void WriteReturn()
		{
			//	Save the return address in a temporary variable
			_fstream.WriteLine("@LCL");
			// D = LCL (address)
			_fstream.WriteLine("D=M");
			_fstream.WriteLine("@5");
			// A = LCL - 5 (address)
			_fstream.WriteLine("A=D-A");
			// D = Value AT [LCL - 5]
			_fstream.WriteLine("D=M");	
			// R15 = Value AT [LCL - 5] (save retAddr)
			_fstream.WriteLine("@R15");
			_fstream.WriteLine("M=D");		
			WritePopD();
			_fstream.WriteLine("@ARG");
			// *ARG, not ARG (RAM[RAM[ARG]], not RAM[ARG])
			_fstream.WriteLine("A=M");
			// *ARG = pop() - Reposition return value for caller
			_fstream.WriteLine("M=D");	
			_fstream.WriteLine("@ARG");
			// D = ARG (address) + 1
			_fstream.WriteLine("D=M+1");	
			_fstream.WriteLine("@SP");
			// SP = ARG (address) + 1 - Restore SP of caller
			_fstream.WriteLine("M=D");		
			// Restore the frame/state of the caller.
			WriteRestorePointer("THAT");	
			WriteRestorePointer("THIS");
			WriteRestorePointer("ARG");
			WriteRestorePointer("LCL");
			_fstream.WriteLine("@R15");
			_fstream.WriteLine("A=M");
			// Jump to the saved return address.
			_fstream.WriteLine("0;JMP");
			_romAddress += 17;
		}

		/// <summary>
		/// Writes the assembly code that affects the function command.
		/// This means that it writes an assembly label containing the functionName,
		/// and assembly code to initialize numLocals local variables to 0 (by pushing 0 numLocals times on to the stack).
		/// </summary>
		/// <param name="functionName">Function name.</param>
		/// <param name="numLocals">Number locals.</param>
		public void WriteFunction(String functionName, int numLocals) 
		{
			_fstream.WriteLine("(" + functionName + ")");
			_currentFunction = functionName;
			for(int i = 0; i < numLocals; i++) 
			{
				_fstream.WriteLine("@SP");
				_fstream.WriteLine("AM=M+1");
				_fstream.WriteLine("A=A-1");
				_fstream.WriteLine("M=0");
				_romAddress += 4;
			}
		}

		/// <summary>
		/// Closes the output file.
		/// If this is not called then nothing will be written to the output file,
		/// due to how streams in C# work.
		/// </summary>
		public void Close()
		{
			_fstream.Close();
		}

		/*
		 * Private and Helper Methods
		 */

		/// <summary>
		/// Generates assembly code to hande the pointer vs. value notation difference.
		/// </summary>
		/// <param name="segment">Segment.</param>
		/// <param name="index">Index.</param>
		private void WriteIndexOffset(string segment, string index) 
		{
			_fstream.WriteLine("@" + segment);
			if (segment == PointerLoc || segment == TempLoc) 
			{
				//  Pointer or Temp = value, not an address.
				_fstream.WriteLine("D=A");
			} 
			else 
			{
				//  All other segments = pointer notation (base + i).
				_fstream.WriteLine("D=M");
			}
			_fstream.WriteLine("@" + index);
			_romAddress += 3;
		}

		/// <summary>
		/// Pushes the value pf a virtual memory segment (pointer) on to the stack.
		/// Used to save the state of the current frame's pointers.
		/// </summary>
		/// <param name="pointer">Pointer.</param>
		private void WritePushPointer(string pointer) 
		{
			_fstream.WriteLine("@" + pointer);
			_fstream.WriteLine("D=M");
			WritePushD();
			_romAddress += 2;
		}
			
		/// <summary>
		/// Writes assembly code to push a value from the D register on to the top of the stack,
		/// and simultaneously update the stack pointer (SP).
		/// It then updates the A-register to the next upper-most value on the stack to perform the binary operation.
		/// </summary>
		/// <param name="op">Op.</param>
		private void WriteBinaryOp(string op) 
		{
			// Update SP.
			WritePopD();
			_fstream.WriteLine("A=A-1");
			_fstream.WriteLine("M=M" + op + "D");
			_romAddress += 2;
		}

		/// <summary>
		/// Writes assembly code to perform the beginning stage of the 2's complement on the top
		/// value of the stack (negation).
		/// The stack pointer (SP) does not need to be adjusted.
		/// </summary>
		private void WriteUnaryOp() 
		{
			_fstream.WriteLine("@SP");
			_fstream.WriteLine("A=M-1");
			_fstream.WriteLine("M=!M");
			_romAddress += 3;
		}

		/// <summary>
		/// Writes assembly code to pop the top-most value off the stack
		/// and store it on the static segment at the specified index.
		/// </summary>
		/// <param name="index">Index.</param>
		private void WritePopToStatic(string index) 
		{
			WritePopD();
			_fstream.WriteLine("@" + GetStaticLabel(index));
			_fstream.WriteLine("M=D");
			_romAddress += 2;
		}

		/// <summary>
		/// Writes the assembly code to pop the top-most value off the stack and
		/// store it on the virtual segment specified at the indicated index.
		/// </summary>
		/// <param name="segment">Segment.</param>
		/// <param name="index">Index.</param>
		private void WritePopToMem(string segment, string index)
		{
			WriteIndexOffset(segment, index);
			_fstream.WriteLine("D=D+A");
			_fstream.WriteLine("@R14");
			_fstream.WriteLine("M=D");
			WritePopD();
			_fstream.WriteLine("@R14");
			_fstream.WriteLine("A=M");
			_fstream.WriteLine("M=D");
			_romAddress += 6;
		}

		/// <summary>
		/// Writes assembly code to pop the top-most value off the stack and store 
		/// it in the D register.
		/// </summary>
		private void WritePopD() 
		{
			_fstream.WriteLine("@SP");
			_fstream.WriteLine("AM=M-1");
			_fstream.WriteLine("D=M");
			_romAddress += 3;
		}

		/// <summary>
		/// Writes the assembly code to push a value from the D register on to the top of the stack.
		/// </summary>
		private void WritePushD() 
		{
			_fstream.WriteLine("@SP");
			_fstream.WriteLine("AM=M+1");
			_fstream.WriteLine("A=A-1");
			_fstream.WriteLine("M=D");
			_romAddress += 4;
		}

		/// <summary>
		/// Writes assembly code to push a value from the indicated index within the specified virtual memory
		/// segment on to the top of the stack.
		/// </summary>
		/// <param name="segment">Segment.</param>
		/// <param name="index">Index.</param>
		private void WritePushMemory(string segment, string index) 
		{
			WriteIndexOffset(segment, index);
			_fstream.WriteLine("A=D+A");
			_fstream.WriteLine("D=M");
			WritePushD();
			_romAddress += 2;
		}

		/// <summary>
		/// Writes assembly code to push a constant on to the top of the stack.
		/// </summary>
		/// <param name="constant">Constant.</param>
		private void WritePushConstant(string constant) 
		{
			_fstream.WriteLine("@" + constant);
			_fstream.WriteLine("D=A");
			WritePushD();
			_romAddress += 2;
		}

		/// <summary>
		/// Writes assembly code to push a value from the indicated index within the static
		/// virtual memory segment on to the top of the stack.
		/// </summary>
		/// <param name="index">Index.</param>
		private void WritePushStatic(string index) 
		{
			_fstream.WriteLine("@" + GetStaticLabel(index));
			_fstream.WriteLine("D=M");
			WritePushD();
			_romAddress += 2;
		}

		/// <summary>
		/// Generates a static label in the form 'currentfile.index' that will be used to reference the static virtual memory segment.
		/// </summary>
		/// <returns>The static label.</returns>
		/// <param name="index">Index.</param>
		private String GetStaticLabel(string index) 
		{
			return _currentFile + "." + index;
		}

		/// <summary>
		/// Generates a label for branching in assembly in the format "_#".
		/// </summary>
		/// <returns>The branch label.</returns>
		private string GetBranchLabel() 
		{
			return "_" + _labelCounter++;
		}

		/// <summary>
		/// Writes assembly code to perform an inequality operation
		/// on the top.
		/// </summary>
		/// <param name="jump">Jump.</param>
		private void WriteInequality(string jump) 
		{
			//	Get unique labels for assembly branching
			string label1 = GetBranchLabel();
			string label2 = GetBranchLabel();
			// Construct assembly code
			// SP is updated to the address of SP - 1
			WritePopD();
			_fstream.WriteLine("A=A-1");
			// D = value of (SP - 1) - value of (SP - 2)
			_fstream.WriteLine("D=D-M");
			_fstream.WriteLine("@" + label1);
			// If condition is true, jump to label1 branch.
			_fstream.WriteLine("D;" + jump);
			// D = false
			_fstream.WriteLine("D=0");          
			_fstream.WriteLine("@" + label2);
			_fstream.WriteLine("0;JMP");
			// Condition is true branch.
			_fstream.WriteLine("(" + label1 + ")");
			// D = true
			_fstream.WriteLine("D=-1");   
			// Condition is false branch.
			_fstream.WriteLine("(" + label2 + ")");
			_fstream.WriteLine("@SP");
			_fstream.WriteLine("A=M-1");
			// Address of (original) SP - 2 = result of comparison (D)
			_fstream.WriteLine("M=D");      
			_romAddress += 11;
		}

		/// <summary>
		/// Restores the specified virtual memory segment of the caller.
		/// </summary>
		/// <param name="pointer">Pointer.</param>
		private void WriteRestorePointer(string pointer)
		{
			/*
			 * The address of LCL is one above the saved state of the calling function.
			 * This means it an be used as a psuedo SP pointer to restore the THAT, THIS,
			 * ARG, and LCL pointers to restore the frame of the caller.
			 */
			_fstream.WriteLine("@LCL");
			// A & RAM[LCL] = RAM[LCL]-1
			_fstream.WriteLine("AM=M-1");
			// D = *(LCL - 1)
			_fstream.WriteLine("D=M");
			_fstream.WriteLine("@" + pointer);
			_fstream.WriteLine("M=D");
			_romAddress += 5;
		}

		/// <summary>
		/// Returns a given label in the format functionName$label.
		/// </summary>
		/// <returns>The proper label.</returns>
		/// <param name="label">Label.</param>
		private string GetProperLabel(string label)
		{
			return _currentFunction + "$" + label;
		}
	}
}

