﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.Generic;

namespace VirtualMachine
{
	/// <summary>
	/// Lexer.
	/// I mostly copied my old one.
	/// </summary>
	public class Lexer
	{
		private string _lines;

		/// <summary>
		/// Initializes a new instance of the <see cref="VirtualMachine.Lexer"/> class.
		/// </summary>
		/// <param name="filename">Filename.</param>
		public Lexer(string filename)
		{
			// Read the entire file and store it to lines.
			_lines = File.ReadAllText(filename);
			CleanLines();
		}

		/// <summary>
		/// Cleans the lines.
		/// </summary>
		private void CleanLines()
		{
			RemoveComments();
			RemoveTabsAndSpaces();
			RemoveEmptyLines();
		}

		/// <summary>
		/// Removes the comments.
		/// </summary>
		private void RemoveComments()
		{
			// http://stackoverflow.com/a/1740692/5415895
			string pattern = @"//.*|(""(?:\\[^""]|\\""|.)*?"")|(?s)/\*.*?\*/";
			_lines = Regex.Replace(_lines, pattern, string.Empty);
		}

		/// <summary>
		/// Removes the tabs and spaces.
		/// </summary>
		private void RemoveTabsAndSpaces()
		{
			// http://stackoverflow.com/a/10032056/5415895
			_lines = Regex.Replace(_lines, @"^\s+|\s+$", string.Empty, RegexOptions.Multiline);
		}

		/// <summary>
		/// Removes the empty lines.
		/// </summary>
		private void RemoveEmptyLines()
		{
			// For some reason this doesn't work on regexpr.com
			_lines = Regex.Replace(_lines, @"^\s+$[\r\n]*", string.Empty, RegexOptions.Multiline).TrimEnd();;
		}

		/// <summary>
		/// Gets the lines.
		/// </summary>
		/// <returns>The lines.</returns>
		public string GetLines()
		{
			return _lines;
		}

		/// <summary>
		/// Just splits the input by line and returns that as a queue.
		/// </summary>
		/// <returns>The commands.</returns>
		public Queue<string> GetCommands()
		{
			// http://stackoverflow.com/a/1508217/5415895
			string[] result = Regex.Split(_lines, "\r\n|\r|\n");
			// https://social.msdn.microsoft.com/Forums/vstudio/en-US/a19ef714-0bc4-4947-8ed2-25891e103aa9/can-i-convert-array-to-queue?forum=csharpgeneral
			return new Queue<string>(result);
		}
	}
}

