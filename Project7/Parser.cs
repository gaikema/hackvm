﻿using System;
using System.Collections.Generic;

namespace VirtualMachine
{
	/// <summary>
	/// Parser.
	/// Handles the parsing of a single .vm file,
	/// and encapsulates access to the input code.
	/// </summary>
	public class Parser
	{
		private Queue<string> _commands;

		/// <summary>
		/// Initializes a new instance of the <see cref="VirtualMachine.Parser"/> class.
		/// </summary>
		/// <param name="filename">Filename.</param>
		public Parser(string filename)
		{
			Lexer luthor = new Lexer(filename);
			_commands = luthor.GetCommands();
		}

		/// <summary>
		/// Determines whether this instance has more commands.
		/// </summary>
		/// <returns><c>true</c> if this instance has more commands; otherwise, <c>false</c>.</returns>
		public bool HasMoreCommands()
		{
			// https://bitbucket.org/gaikema/hackassembler/src/4f61e5fcc5d156f98b78e61070ca2c7646c0f7bf/Assembler/Parser.cs?at=master&fileviewer=file-view-default#Parser.cs-167
			return (_commands.Count != 0);
		}

		/// <summary>
		/// Reads the next command from the input and makes it the current command.
		/// </summary>
		public void Advance()
		{
			_commands.Dequeue();
		}

		/// <summary>
		/// Gets the current command.
		/// </summary>
		/// <returns>The command.</returns>
		public string GetCommand()
		{
			return _commands.Peek();
		}

		/// <summary>
		/// Gets the type of the command.
		/// </summary>
		/// <returns>The command type.</returns>
		public CommandType GetCommandType()
		{
			string str = _commands.Peek();
			// http://stackoverflow.com/a/6111355/5415895
			string[] args = str.Split(null);
			switch (args[0])
			{
				case "pop":
					return CommandType.C_POP;
				case "push":
					return CommandType.C_PUSH;
				case "label":
					return CommandType.C_LABEL;
				case "goto":
					return CommandType.C_GOTO;
				case "if":
				case "if-goto":
					return CommandType.C_IF;
				case "function":
					return CommandType.C_FUNCTION;
				case "return":
					return CommandType.C_RETURN;
				case "call":
					return CommandType.C_CALL;
				default:
					return CommandType.C_ARITHMETIC;
			}
		}

		/// <summary>
		/// Returns the first the first argument of the current command.
		/// </summary>
		/// <returns>The arg1.</returns>
		public string GetArg1()
		{
			string str = _commands.Peek();
			CommandType ctype = GetCommandType();
			string[] args = str.Split(null);

			switch (ctype)
			{
				case CommandType.C_RETURN:
					throw new Exception("Invalid command type");
				case CommandType.C_ARITHMETIC:
					return args[0];
				default:
					return args[1];
			}
		}

		/// <summary>
		/// Returns the second argument of the current command.
		/// </summary>
		/// <returns>The arg2.</returns>
		public string GetArg2()
		{
			string str = _commands.Peek();
			CommandType ctype = GetCommandType();
			string[] args = str.Split(null);

			if (!(ctype == CommandType.C_POP || ctype == CommandType.C_PUSH || ctype == CommandType.C_FUNCTION || ctype == CommandType.C_CALL))
				throw new Exception("Invalid command type");

			return args[2];
		}
	}
}

