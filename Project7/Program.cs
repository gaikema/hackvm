﻿using System;
using System.IO;

namespace VirtualMachine
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			// Not enough arguments.
			if (args.Length < 1)
			{
				Console.WriteLine("Insufficient arguments.");
				return;
			}

			string file = args[0];
			Parser parser;
			CodeWriter writer;

			// Single file passed.
			if (Path.GetExtension(file) == ".vm")
			{
				Console.WriteLine("Single file passed: " + file);

				parser = new Parser(file);
				writer = new CodeWriter(Path.GetFileNameWithoutExtension(file) + ".asm");
				writer.SetFileName(file);
				Translate(ref parser, ref writer);
				writer.Close();
				Console.WriteLine("Output written to " + Path.GetFileNameWithoutExtension(file) + ".asm");
			} 
			// Directory passed.
			else
			{
				// https://msdn.microsoft.com/en-us/library/system.io.directoryinfo.name(v=vs.110).aspx
				DirectoryInfo dir = new DirectoryInfo(file);
				String dirName = dir.Name;
				Console.WriteLine("Directory passed: " + dirName);

				writer = new CodeWriter(Path.Combine(file,dirName + ".asm"));

				string [] fileEntries = Directory.GetFiles(file);
				foreach (string fileName in fileEntries)
				{
					if (Path.GetExtension(fileName) == ".vm")
					{
						writer.SetFileName(fileName);
						parser = new Parser(fileName);
						Translate(ref parser, ref writer);
					}
				}

				writer.Close();
				Console.WriteLine("Output written to " + dirName + ".asm");
			}
		}

		public static void Translate(ref Parser parser, ref CodeWriter writer)
		{
			string command, arg1, arg2;
			CommandType ctype;

			while (parser.HasMoreCommands())
			{
				command = parser.GetCommand();
				//arg1 = parser.GetArg1();
				//arg2 = parser.GetArg2();
				ctype = parser.GetCommandType();
				switch (ctype)
				{
					case CommandType.C_ARITHMETIC:
						writer.WriteArithmetic(command);
						break;
					case CommandType.C_POP:
					case CommandType.C_PUSH:
						arg1 = parser.GetArg1();
						arg2 = parser.GetArg2();
						writer.WritePushPop(ctype, arg1, arg2);
						break;
					case CommandType.C_LABEL:
						arg1 = parser.GetArg1();
						writer.WriteLabel(arg1);
						break;
					case CommandType.C_GOTO:
						arg1 = parser.GetArg1();
						writer.WriteGoto(arg1);
						break;
					case CommandType.C_IF:
						arg1 = parser.GetArg1();
						writer.WriteIf(arg1);
						break;
					case CommandType.C_CALL:
						arg1 = parser.GetArg1();
						arg2 = parser.GetArg2();
						writer.WriteCall(arg1, arg2);
						break;
					case CommandType.C_FUNCTION:
						arg1 = parser.GetArg1();
						arg2 = parser.GetArg2();
						writer.WriteFunction(arg1, Int32.Parse(arg2));
						break;
					case CommandType.C_RETURN:
						writer.WriteReturn();
						break;
					default:
						Console.WriteLine("This should never happen.");
						break;
				}
				parser.Advance();
			}
		}
	}
}
