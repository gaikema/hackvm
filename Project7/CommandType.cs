﻿using System;

namespace VirtualMachine
{
	/// <summary>
	/// Command type.
	/// </summary>
	public enum CommandType
	{
		/// <summary>
		/// Returned for all artithmetic commands.
		/// </summary>
		C_ARITHMETIC,
		C_PUSH,
		C_POP,
		C_LABEL,
		C_GOTO,
		C_IF,
		C_FUNCTION,
		C_RETURN,
		C_CALL
	}
}

